# Steam Provider Library

## Usage

### Install the dependency

```
npm install steam-provider 
```

### Import the library

#### TypeScript
```TypeScript
import { SteamProvider } from 'steam-provider';
```

#### JavaScript
```JavaScript
var steam = require('steam-provider')
```

### Search for a game (Limited to 10 first results)

#### TypeScript
```TypeScript
let provider = new SteamProvider();

provider.search("Portal 2", 10, "en", "us").then(result => {
    console.log(result);
})
```

#### JavaScript
```JavaScript
var provider = new steam.SteamProvider();

provider.search("Portal 2", 10, "en", "us").then(result => {
    console.log(result)
})
```

### Search Results
```JavaScript
[ SteamSearchEntry {
    id: '620',
    name: 'Portal 2',
    url:
     'https://store.steampowered.com/app/620/Portal_2/?snr=1_7_7_151_150_1',
    price: '19.99',
    priceWithDiscount: '1.99',
    score:
     'Overwhelmingly Positive, 98% of the 89,727 user reviews for this game are positive.' },
  SteamSearchEntry {
    id: '104600',
    name: 'Portal 2 - The Final Hours',
    url:
     'https://store.steampowered.com/app/104600/Portal_2__The_Final_Hours/?snr=1_7_7_151_150_1',
    price: '1.99',
    priceWithDiscount: '0.49',
    score:
     'Very Positive, 81% of the 396 user reviews for this game are positive.' },
  SteamSearchEntry {
    id: '323180',
    name: 'Portal 2 Soundtrack',
    url:
     'https://store.steampowered.com/app/323180/Portal_2_Soundtrack/?snr=1_7_7_151_150_1',
    price: '',
    priceWithDiscount: '',
    score:
     'Positive, 92% of the 13 user reviews for this game are positive.' },
  SteamSearchEntry {
    id: '660',
    name: 'Portal 2 Sixense MotionPack DLC',
    url:
     'https://store.steampowered.com/app/660/Portal_2_Sixense_MotionPack_DLC/?snr=1_7_7_151_150_1',
    price: '',
    priceWithDiscount: '',
    score: '' },
  SteamSearchEntry {
    id: '247120',
    name: 'Portal 2 Sixense Perceptual Pack',
    url:
     'https://store.steampowered.com/app/247120/Portal_2_Sixense_Perceptual_Pack/?snr=1_7_7_151_150_1',
    price: '',
    priceWithDiscount: '',
    score: '' },
  SteamSearchEntry {
    id: '400',
    name: 'Portal',
    url:
     'https://store.steampowered.com/app/400/Portal/?snr=1_7_7_151_150_1',
    price: '9.99',
    priceWithDiscount: '0.99',
    score:
     'Overwhelmingly Positive, 97% of the 35,114 user reviews for this game are positive.' },
  SteamSearchEntry {
    id: '317400',
    name: 'Portal Stories: Mel',
    url:
     'https://store.steampowered.com/app/317400/Portal_Stories_Mel/?snr=1_7_7_151_150_1',
    price: '',
    priceWithDiscount: '',
    score:
     'Overwhelmingly Positive, 95% of the 9,457 user reviews for this game are positive.' },
  SteamSearchEntry {
    id: '684410',
    name: 'Bridge Constructor Portal',
    url:
     'https://store.steampowered.com/app/684410/Bridge_Constructor_Portal/?snr=1_7_7_151_150_1',
    price: '9.99',
    priceWithDiscount: '7.99',
    score:
     'Very Positive, 89% of the 1,055 user reviews for this game are positive.' },
  SteamSearchEntry {
    id: '374040',
    name: 'Portal Knights',
    url:
     'https://store.steampowered.com/app/374040/Portal_Knights/?snr=1_7_7_151_150_1',
    price: '19.99',
    priceWithDiscount: '11.99',
    score:
     'Very Positive, 82% of the 9,437 user reviews for this game are positive.' },
  SteamSearchEntry {
    id: '446750',
    name: 'Portal Stories: VR',
    url:
     'https://store.steampowered.com/app/446750/Portal_Stories_VR/?snr=1_7_7_151_150_1',
    price: '',
    priceWithDiscount: '',
    score:
     'Very Positive, 93% of the 785 user reviews for this game are positive.' } ]

```

### Get game details

#### TypeScript
```TypeScript
let provider = new SteamProvider();

provider.detail("620").then(result => {
    console.log(result);
})
```

#### JavaScript
```JavaScript
var provider = new steam.SteamProvider();

provider.detail("620").then(result => {
    console.log(result)
})
```

### Detail Results
```JavaScript
SteamDetailEntry {
  id: '620',
  name: 'Portal 2',
  genres: [ 'Action', 'Adventure' ],
  priceData:
   PriceInfo {
     initialPrice: '19.99',
     finalPrice: '19.99',
     discountPercent: 0 },
  otherData:
   OtherInfo {
     imageUrl: 'https://steamcdn-a.akamaihd.net/steam/apps/620/header.jpg?t=1512411524',
     platforms: [ 'Windows', 'MacOS', 'Linux' ],
     metacriticScore: 95,
     features:
      [ 'Single-player',
        'Co-op',
        'Steam Achievements',
        'Full controller support',
        'Steam Trading Cards',
        'Captions available',
        'Steam Workshop',
        'Steam Cloud',
        'Stats',
        'Includes level editor',
        'Commentary available' ],
     developer: [ 'Valve' ],
     publisher: [ 'Valve' ] } }
```

## Notes
By default the library will recover data in US English and will get the prices in USD currency.

You can choose the language and the currency optionally.

```JavaScript
provider.search("Portal 2", "spanish", "es").then(result => {
    console.log(result);
})

provider.detail("620", "spanish", "es").then(result => {
    console.log(result)
})
```

## Known Issues

## About & Credits

[steam-provider](https://github.com/alosarjos/steam-provider) provides basic functionality for parsing Steam games information.

Credits to: [ckatzorke](https://github.com/ckatzorke/howlongtobeat) for providing me an excelent starting point for this project

import { expect } from 'chai';

import { SteamSearchEntry, SteamDetailEntry } from '../../src/services/steam/models/steam-entry';
import { SteamProvider } from '../../src/services/steam/providers/steam-provider';

let provider = new SteamProvider();
let searchResult: Array<SteamSearchEntry>;
let detailResult: SteamDetailEntry;

describe('Steam provider search() method', () => {
    it('Should return a SteamEntry array', () => {
        let result: Promise<Array<SteamSearchEntry>> = new Promise<Array<SteamSearchEntry>>((resolve, reject) => {
            provider.search('Portal 2').then((result) => {
                expect(result).to.be.an('array');
                expect(result).to.not.be.empty;
                searchResult = result;
                resolve(result);
            });
        });
        return result;
    }).timeout(100000);

    it('Should have the Portal 2 partial element in the array', () => {
        expect(searchResult[0].$id).to.equal('620');
        expect(searchResult[0].$name).to.equal('Portal 2');
    });
});

describe('Steam provider detail() method', () => {
    it('Should return a SteamEntry element', () => {
        let result: Promise<SteamDetailEntry> = new Promise<SteamDetailEntry>((resolve, reject) => {
            provider.detail('620').then((result) => {
                expect(result).not.to.be.empty;
                detailResult = result;
                resolve(result);
            });
        });
        return result;
    }).timeout(100000);

    it('Should be the Portal 2 SteamEntry element', () => {
        expect(detailResult.$id).to.equal('620');
        expect(detailResult.$name).to.equal('Portal 2');
        expect(detailResult.$otherData.$developer[0]).to.equal('Valve');
        expect(detailResult.$otherData.$publisher[0]).to.equal('Valve');
        expect(detailResult.$otherData.$platforms).to.have.lengthOf(3);
        expect(detailResult.$otherData.$metacriticScore).to.equal(95);
        expect(detailResult.$otherData.$features).to.have.lengthOf(11);
    });
});

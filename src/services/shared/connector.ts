/**
 * HTTP request options class
 */
export class HttpRequestOptions {
    private hostname: string;
    private port: number;
    private method: string;
    private path: string;
    private headers: Object;

    constructor() { }

    public setOptions(requestOptions) {
        this.hostname = requestOptions.hostname;
        this.port = requestOptions.port;
        this.method = requestOptions.method;
        this.path = requestOptions.path;
        this.headers = requestOptions.headers;
    }

    public set $path(value: string) {
        this.path += value;
    }

    public get $path(): string {
        return this.path;
    }

    public getOptionsObject(): Object {
        return {
            hostname: this.hostname,
            port: this.port,
            method: this.method,
            path: this.path,
            headers: this.headers
        };
    }
}

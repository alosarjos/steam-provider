/**
 * Class for the info for each search result
 */
export class SteamSearchEntry {
    private id: string;
    private name: string;
    private url: string;
    private price: string;
    private priceWithDiscount: string;
    private score: string;

    constructor($id: string, $name: string, $url: string, $price: string, $priceWithDiscount: string, $score: string) {
        this.id = $id;
        this.name = $name;
        this.url = $url;
        this.price = $price;
        this.priceWithDiscount = $priceWithDiscount;
        this.score = $score;
    }

    public get $id(): string {
        return this.id;
    }

    public get $name(): string {
        return this.name;
    }

    public get $url(): string {
        return this.url;
    }

    public get $price(): string {
        return this.price;
    }

    public get $priceWithDiscount(): string {
        return this.priceWithDiscount;
    }

    public get $score(): string {
        return this.score;
    }
}

/**
 * Class for the details  of each game
 */
export class SteamDetailEntry {
    private id: string;
    private name: string;
    private genres: Array<string>;
    private priceData: PriceInfo;
    private otherData: OtherInfo;

    constructor($id: string, $name: string, $genres: Array<string>, $priceData: PriceInfo, $otherData: OtherInfo) {
        this.id = $id;
        this.name = $name;
        this.genres = $genres;
        this.priceData = $priceData;
        this.otherData = $otherData;
    }

    public get $id(): string {
        return this.id;
    }

    public get $name(): string {
        return this.name;
    }

    public get $genres(): Array<string> {
        return this.genres;
    }

    public get $priceData(): PriceInfo {
        return this.priceData;
    }

    public get $otherData(): OtherInfo {
        return this.otherData;
    }
}

/**
 * Other info inside SteamDetailEntry
 */
export class OtherInfo {
    private imageUrl: string;
    private platforms: Array<string>;
    private metacriticScore: number;
    private features: Array<string>;
    private developer: Array<string>;
    private publisher: Array<string>;

    constructor(
        $imageUrl: string,
        $platforms: Array<string>,
        $metacriticScore: number,
        $features: Array<string>,
        $developer: Array<string>,
        $publisher: Array<string>
    ) {
        this.imageUrl = $imageUrl;
        this.platforms = $platforms;
        this.metacriticScore = $metacriticScore;
        this.features = $features;
        this.developer = $developer;
        this.publisher = $publisher;
    }

    public get $imageUrl(): string {
        return this.imageUrl;
    }

    public get $platforms(): Array<string> {
        return this.platforms;
    }

    public get $metacriticScore(): number {
        return this.metacriticScore;
    }

    public get $features(): Array<string> {
        return this.features;
    }

    public get $developer(): Array<string> {
        return this.developer;
    }

    public get $publisher(): Array<string> {
        return this.publisher;
    }
}

/**
 * Pricing info inside SteamDetailEntry
 */
export class PriceInfo {
    private initialPrice: string;
    private finalPrice: string;
    private discountPercent: number;

    constructor($initialPrice: string, $finalPrice: string, $discountPercent: number) {
        this.initialPrice = $initialPrice;
        this.finalPrice = $finalPrice;
        this.discountPercent = $discountPercent;
    }

    public get $initialPrice(): string {
        return this.initialPrice;
    }

    public get $finalPrice(): string {
        return this.finalPrice;
    }

    public get $discountPercent(): number {
        return this.discountPercent;
    }
}

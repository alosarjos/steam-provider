import * as cheerio from 'cheerio';
import { SteamConnector } from './steam-connector';
import { SteamSearchEntry, SteamDetailEntry, OtherInfo, PriceInfo } from '../models/steam-entry';
import { SteamProvider } from './steam-provider';

/**
 * Data parsers class
 */
export class SteamParser {
	/**
   * Details data parser
   * @param jsonString JSON in string format to parse
   * @param id App ID of the game
   */
	static parseDetails(jsonString: string, id: string): SteamDetailEntry {
		let gameName = '';
		let imageUrl = '';
		let developer = new Array<string>();
		let publisher = new Array<string>();
		let platforms = new Array<string>();
		let features = new Array<string>();
		let genres = new Array<string>();
		let metacriticScore = 0;
		let rawInitialPrice = '';
		let initialPrice = '';
		let rawFinalPrice = '';
		let finalPrice = '';
		let discountPercent = 0;

		let json = JSON.parse(jsonString);
		gameName = json[id].data.name;
		imageUrl = json[id].data.header_image;
		developer = json[id].data.developers;
		publisher = json[id].data.publishers;

		json[id].data.genres.forEach((element) => {
			genres.push(element.description);
		});

		metacriticScore = json[id].data.metacritic != null ? json[id].data.metacritic.score : null;
		if (json[id].data.price_overview == null) {
			// Free to play
			initialPrice = '0.0';
			finalPrice = '0.0';
			discountPercent = 0;
		} else {
			rawInitialPrice = String(json[id].data.price_overview.initial);
			initialPrice =
				rawInitialPrice.substr(0, rawInitialPrice.length - 2) +
				'.' +
				rawInitialPrice.substr(rawInitialPrice.length - 2);
			rawFinalPrice = String(json[id].data.price_overview.final);
			finalPrice =
				rawFinalPrice.substr(0, rawFinalPrice.length - 2) +
				'.' +
				rawFinalPrice.substr(rawFinalPrice.length - 2);
			discountPercent = json[id].data.price_overview.discount_percent;
		}

		json[id].data.categories.forEach((element) => {
			features.push(element.description);
		});

		if (json[id].data.platforms.windows === true) platforms.push('Windows');

		if (json[id].data.platforms.mac === true) platforms.push('MacOS');

		if (json[id].data.platforms.linux === true) platforms.push('Linux');

		return new SteamDetailEntry(
			id,
			gameName,
			genres,
			new PriceInfo(initialPrice, finalPrice, discountPercent),
			new OtherInfo(imageUrl, platforms, metacriticScore, features, developer, publisher)
		);
	}

	/**
   * Search data parser
   * @param html HTML website to parse
   */
	static parseSearch(html: string, elementNumber: number): Array<SteamSearchEntry> {
		let results: Array<SteamSearchEntry> = new Array<SteamSearchEntry>();

		const $ = cheerio.load(html);
		let gameList = $('#search_result_container').find('a');

		gameList.each((i, element) => {
			if (results.length == elementNumber) return false;

			let gameUrl: string = '';
			let gameId: string = '';
			let gameName: string = '';
			let gamePrice: string = '';
			let gamePriceWithDiscount = '';
			let gameScore: string = '';
			let gameEntry: Cheerio = $('#search_result_container').find('a').eq(i);

			gameUrl = gameEntry.attr('href');
			gameId = gameUrl.substring(gameUrl.indexOf('app/') + 4);
			gameId = gameId.substring(0, gameId.indexOf('/'));
			gameName = gameEntry.find('.title').text().trim();

			gamePrice = gameEntry.find('.search_price').first().text().trim();
			//Check for discounts
			if (/[0-9.,]*[^0-9.,][0-9.,]*[0-9.,]*[^0-9.,][0-9.,]*/g.test(gamePrice)) {
				//If symbol is on the left side

				//If sumbol is on the right side
				gamePriceWithDiscount = gamePrice
					.substring(gamePrice.substring(1).search(/[^0-9.,]/) + 1)
					.replace(/[^0-9.,]/g, '')
					.trim();
				gamePrice = gamePrice
					.substring(0, gamePrice.substring(1).search(/[^0-9.,]/) + 1)
					.replace(/[^0-9.,]/g, '')
					.trim();
			} else {
				gamePriceWithDiscount = '';
				gamePrice = gameEntry.find('.search_price').first().first().text().replace(/[^0-9.,]/g, '').trim();
			}
			gameScore = gameEntry.find('.search_review_summary').attr('data-store-tooltip'.trim());
			gameScore = gameScore !== undefined ? gameScore.replace(/<br>/, ', ') : '';
			results.push(new SteamSearchEntry(gameId, gameName, gameUrl, gamePrice, gamePriceWithDiscount, gameScore));
		});
		return results;
	}
}

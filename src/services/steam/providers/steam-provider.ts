import { SteamSearchEntry, SteamDetailEntry } from '../models/steam-entry';
import { SteamConnector } from './steam-connector';
import { SteamParser } from './steam-parser';

/**
 * Class in charge of providing public methods
 */
export class SteamProvider {
	private steamConnector: SteamConnector;

	constructor() {
		this.steamConnector = new SteamConnector();
	}

	/**
     * Search through the Steam searching website. Results are limited to 10 for now.
     * @param gameName Name to search
     * @param language Language for the results (English by default)
     * @param currency Currency for the results (USD by default)
     */
	public async search(
		gameName: string,
		elementNumber: number = 10,
		language: string = 'english',
		currency: string = 'us',
		pageNumber: number = 1
	): Promise<Array<SteamSearchEntry>> {
		let result = new Array<SteamSearchEntry>();

		while (elementNumber > 25) {
			let searchPage = await this.steamConnector.search(gameName, pageNumber++, language, currency);
			result = result.concat(SteamParser.parseSearch(searchPage, elementNumber));
			elementNumber -= 25;
		}

		if (elementNumber > 0) {
			let searchPage = await this.steamConnector.search(gameName, pageNumber, language, currency);
			result = result.concat(SteamParser.parseSearch(searchPage, elementNumber));
		}

		return result;
	}

	/**
     * 
     * @param appId Steam AppId to retrieve it's detailts
     * @param language Language for the results (English by default)
     * @param currency Currency for the results (USD by default)
     */
	public async detail(
		appId: string,
		language: string = 'english',
		currency: string = 'us'
	): Promise<SteamDetailEntry> {
		let detailRawData = await this.steamConnector.detail(appId, language, currency);
		let entry = SteamParser.parseDetails(detailRawData, appId);
		return entry;
	}
}
